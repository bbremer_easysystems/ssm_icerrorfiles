#################################################################################################################
# Title:	ImageCapture_ErrorFiles_AWS
# Description:	Dynamically retrieve file count from IC error folders for AWS CloudWatch.
# Author:	Bas Bremer
# Date:		2019-03-19
# Version:	1.0
#
# Changelog:
# Date		| By	| Description
# 20190319	| BB	| Initial
# 20190404	| BB	| Added fnmatch to count specific file type
#
#################################################################################################################

# Import OS information
import os
import fnmatch
import sys

# declare parameter status and set default value
status = 0

# declare parameter filecounter and set default value
filecounter = 0

#Retrieve direcotry from powershell script
directory = sys.argv[1]

#Check if directory exists
directoryExists = os.path.exists(directory)
if directoryExists == 0:
    exit(3)

# Set directory and retrieve subdirectories (which are customer folders)
subdirectory = [os.path.join(directory , subdirectory)
    for subdirectory in os.listdir(directory) if os.path.isdir(os.path.join(directory , subdirectory))]

# Open loop to loop through error folders
clientfolder = 0

while clientfolder < len(subdirectory):
    # Print statement for test purposes
    # print(subdirectory[clientfolder])
    folderlist = os.listdir(subdirectory[clientfolder])
    folderlistnew = [k for k in folderlist if 'Error' in k]
    # Loop trough error folders and retrieve count of errored files
    x = 0
    while x < len(folderlistnew):
        files = (subdirectory[clientfolder] + '/' + folderlistnew[x])
        number_files = len(fnmatch.filter(os.listdir(files), '*.pdf'))
        filecounter += number_files
        # Import boto3 for AWS CloudWatch
        import boto3
        # Create CloudWatch client
        cloudwatch = boto3.client('cloudwatch', region_name='eu-west-2')
        # Put custom metrics
        cloudwatch.put_metric_data(
            MetricData=[
                    {
                        'MetricName': 'ERRORED_FILES_PER_CUSTOMER',
                        'Dimensions': [
                            {
                                'Name': 'ERRORED_FILES',
                                'Value': files
                            },
                        ],
                        'Unit': 'None',
                        'Value': number_files
                    },
                ],
                Namespace='SITE/TRAFFIC'
        )
        x += 1
    clientfolder += 1
    status = 1

#End of script

if status ==  0:
    exit(1)
elif filecounter  >   0:
    exit(2)
else:
    exit()
