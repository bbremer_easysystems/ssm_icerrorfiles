<########################################################################################################################################
# Title:	    main.ps1
# Description:	Main powershell script for dynamic retrieve of file count from IC error folders for AWS CloudWatch.
# Author:	    Bas Bremer
# Date:		    2019-06-14
# Version:  	1.0
#
# Changelog:
# Date		| By	| Description
# 20190614	| BB	| Initial
#
########################################################################################################################################>

<# Parameters #>
#Main information
$file1          =   'main.ps1'
$file2          =   'sendtoteams.ps1'
$file3          =   'icerrorfiles.py'

#Diretories
$scriptdir      =   'C:/Temp'
$icdirectory    =   'C:/ImageCapture/Import/'
$pydir          =   'C:/Python37/python.exe'
$file1dir       =   $scriptdir + '/' + $file1
$file2dir       =   $scriptdir + '/' + $file2
$file3dir       =   $scriptdir + '/' + $file3

#Teams
$channel        =   'https://outlook.office.com/webhook/96ec7763-fda0-4975-95af-f6425e73dda1@612a125a-c5e7-40b8-9ee2-19a1f436db46/IncomingWebhook/19c1692bb0e04451844358d7dcd41731/baed1c78-23b5-4db4-b274-caa92e3fc4f7'

#Public ip
$publicip       =   (invoke-WebRequest -uri "http://ifconfig.me/ip" -UseBasicParsing).Content

#Error messages
$message0       =   'Op server: ' + $publicip +' is script: ' + $file3 + ' succesvol uitgevoerd en geen bestanden.'
$message1       =   'Op server: ' + $publicip +' is script: ' + $file3 + ' niet succesvol uitgevoerd.'
$message2       =   'Op server: ' + $publicip +' is script: ' + $file3 + ' succesvol uitgevoerd en er zijn bestanden.'
$message3       =   'Op server: ' + $publicip +' is script: ' + $file3 + ' uitgevoerd, maar kan de map: ' + $icdirectory + ' niet uitlezen.'

#Import Teams functionality
. $file2dir

<# Main Script#>
cd $scriptdir
$Process = Start-Process "$pydir" -ArgumentList "$file3dir", "$icdirectory" -NoNewWindow -Wait -PassThru

<# Errorhandling#>
If ($Process.ExitCode -eq 0) {
    Send-toTeams -webhook $channel -text $message0
}
If ($Process.ExitCode -eq 1) {
    Send-toTeams -webhook $channel -text $message1
}
If ($Process.ExitCode -eq 2) {
    Send-toTeams -webhook $channel -text $message2
}
If ($Process.ExitCode -eq 3) {
    Send-toTeams -webhook $channel -text $message3
}

<#End of script#>