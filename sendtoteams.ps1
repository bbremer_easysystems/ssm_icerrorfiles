<#
.SYNOPSIS
    Powershell Function for sending a notification to a Microsoft Teams channel.
.DESCRIPTION
    This Powershell function can be used for sending a notification to a Microsoft Teams channel. The function takes the input from the $text variable and converts it to a compatible JSON payload to be delivered to the specified Microsoft Teams webhook.
.EXAMPLE
    PS C:\> Send-toTeams -webhook $channel -text "Hello World!"
    Sends the text "Hello World!" to the channel associated with the webhook stored in the $channel variable.
.PARAMETER webhook
    Specifies the URL of the Webhook provided by the Microsoft Teams Webhook Connector.
.PARAMETER text
    Specifies the text which you wish to be send to the Microsoft Teams channel.
#>
function Send-toTeams {
    param (
    [Parameter(Mandatory=$true)]
    $webhook,
    [Parameter(Mandatory=$true)]
    $text
    )
    $payload = @{
        "text" = $text
    }
    $json = ConvertTo-Json $payload
    Invoke-RestMethod -Method post -ContentType 'Application/Json' -Body $json -Uri $webhook
}